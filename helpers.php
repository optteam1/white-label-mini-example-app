<?php
 class AccredifyHelpers{
 	public $AccredifyAPI;

 	public function __construct(Accredify\API $AccredifyAPI){
 		$this->AccredifyAPI = $AccredifyAPI;
	}

	//These are some example helpers
	public function geUserQuestions($is_new=true){
		$AccredifyAPI = $this->AccredifyAPI;
		if($is_new){
 			$results = $AccredifyAPI::newUserQuestions();
 		}else{
			if(!isset($_REQUEST['access_token'])){
				header( 'Location: /newForm.php' ) ; 	
			} 			
 			$results = $AccredifyAPI::updateUserQuestions($_REQUEST['access_token']); 			
 		}
		return $results['data'];//Certificate Form Elements
	}

	public function getElements($type){
		if(!isset($_REQUEST['access_token'])){
			header( 'Location: /newForm.php' ) ; 	
		}
		
		$AccredifyAPI = $this->AccredifyAPI;		
 		$results = $AccredifyAPI::getQuestions($type,$_REQUEST['access_token']);
		return $results['data'];//Certificate Form Elements
	}

	public function inputBuilder($inputObj,$with_wrapper=false,$custom_wrapper_classes=''){
		$domElement="";
		switch ($inputObj['input']) {
			case 'text':
				$domElement = ($with_wrapper)? $this->elementInputWrapper($inputObj,$custom_wrapper_classes) : $this->elementInput($inputObj);
			break;
			case 'password':
				$domElement = ($with_wrapper)? $this->elementInputWrapper($inputObj,$custom_wrapper_classes) : $this->elementInput($inputObj);
			break;
			case 'email':
				$domElement = ($with_wrapper)? $this->elementInputWrapper($inputObj,$custom_wrapper_classes) : $this->elementInput($inputObj);
			break;			
			case 'file':
				$domElement = ($with_wrapper)? $this->elementInputWrapper($inputObj,$custom_wrapper_classes) : $this->elementInput($inputObj);
			break;					
			case 'select':
				$domElement = ($with_wrapper)? $this->elementSelectWrapper($inputObj,$custom_wrapper_classes) : $this->elementSelect($inputObj);					
			break;
			case 'hidden':
				$domElement = $this->elementInput($inputObj);
			break;	
			case 'property':
				$domElement = $this->elementProperty($inputObj);
			break;
			case 'tabel':
				$domElement = $this->elementTable($inputObj);
			break;
			case 'container':
				$domElement = $this->elementContainer($inputObj);
			break;
			case 'canvas':
				$domElement = $this->elementCanvas($inputObj);
			break;

		}


		return $domElement;
	}

	public function elementInput($inputObj){
			$value = (isset($inputObj['value']))? "value='".$inputObj['value']."'" : "";//If value is sent
			$required = ($inputObj['required'])? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation				
			$domElement = "<input type='".$inputObj['input']."' name='".$inputObj['name']."' ".$value." class='".$inputObj['class']."' placeholder='".$inputObj['placeholder']."' ".$required." >";
			return $domElement;
	}

	public function elementInputWrapper($inputObj,$custom_wrapper_classes){
		$required = ($inputObj['required'])? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation				
		
		$domElement = "";
		//Required Class => accredify_input_container
		//Required Attribute => data-accredify-conditional=''
		$conditional = isset($inputObj['conditional'])? json_encode($inputObj['conditional']) : "";
		$domElement .= "<div class='".$custom_wrapper_classes." accredify_input_container' data-accredify-conditional='".$conditional."'>";
			$domElement .= "<label>".$inputObj['label']."</label>";				
			$domElement .= $this->elementInput($inputObj);

			//If we need a container to add objects to from conditional statments
			if(isset($inputObj['container_addon'] )){
				$domElement .= "<div id='".$inputObj['container_addon']."'></div>";

			}

		$domElement .= "</div>";
		return $domElement;
	}

	public function elementSelect($inputObj){
		$required = ($inputObj['required'])? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation				
		
		$domElement = "";		
		$domElement .= "<select name='".$inputObj['name']."' class='".$inputObj['class']."' ".$required.">";
		$domElement .= "<option value=''>Select an Option</option>";
		foreach($inputObj['options'] as $key=>$value):
			$selected = (isset($inputObj['value']) && $inputObj['value'] == $key)? "selected='selected'" : "";//If value is sent

			$domElement .= "<option value='".$key."' ".$selected.">".$value."</option>";
		endforeach;
		$domElement .= "</select>";
		return $domElement;		
	}

	public function elementSelectWrapper($inputObj,$custom_wrapper_classes){
		$required = ($inputObj['required'])? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation			
		
		$domElement = "";		
		//Required Class => accredify_input_container
		//Required Attribute => data-accredify-conditional=''
		$conditional = isset($inputObj['conditional'])? json_encode($inputObj['conditional']) : "";
		$domElement .= "<div class='".$custom_wrapper_classes." accredify_input_container' data-accredify-conditional='".$conditional."'>";
			$domElement .= "<label>".$inputObj['label']."</label>";
			$domElement .= $this->elementSelect($inputObj);
			
			//If we need a container to add objects to from conditional statments
			if(isset($inputObj['container_addon'] )){
				$domElement .= "<div id='".$inputObj['container_addon']."'></div>";

			}

		$domElement .= "</div>";
		return $domElement;
	}

	public function propertyFilter($fileds,$template_ident){
		foreach($fileds as $field){
			if($field['template_ident'] == $template_ident){
				return $field;
				break;
			}
		}
		return null;
	}

	public function elementProperty($inputObj){
			$domElement = "";
			$domElement .= "<div id='".$inputObj['id']."' class='row ".$inputObj['class']."'>";
			$domElement .= "<label>".$inputObj['label']."</label>";
						
			$property_fileds = $inputObj['template']['value'];
			$street_address = $this->propertyFilter($property_fileds,"street_address"); 
			$appt_suite = $this->propertyFilter($property_fileds,"appt_suite");
			$city = $this->propertyFilter($property_fileds,"city");
			$state = $this->propertyFilter($property_fileds,"state");
			$zip = $this->propertyFilter($property_fileds,"zip");
			$state = $this->propertyFilter($property_fileds,"state");
			$value = $this->propertyFilter($property_fileds,"value");
			$mortgage = $this->propertyFilter($property_fileds,"mortgage");
			$is_owned = $this->propertyFilter($property_fileds,"is_owned");

			//Street Address		
			$domElement .= "<div class='col-sm-12'>";
				$domElement .= "<label>".$street_address['label']."</label><br/>";							
				$domElement .= $this->inputBuilder($street_address);
			$domElement .= "</div>";

			//Apartment/Suite
			$appt_elm_size = ($is_owned == null)? 'col-sm-12' : 'col-sm-8';//Is the question of ownership asked? Adjust look.
			$domElement .= "<div class='".$appt_elm_size."'>";
				$domElement .= "<label>".$appt_suite['label']."</label><br/>";										
				$domElement .= $this->inputBuilder($appt_suite);
			$domElement .= "</div>";

			//Own or Rent?
			if($is_owned != null){
				$domElement .= "<div class='col-sm-4'>";
					$domElement .= "<label>".$is_owned['label']."</label><br/>";										
					$domElement .= $this->inputBuilder($is_owned);	
				$domElement .= "</div>";
			}

			//City
			$domElement .= "<div class='col-sm-4'>";
				$domElement .= "<label>".$city['label']."</label><br/>";										
				$domElement .= $this->inputBuilder($city);
			$domElement .= "</div>";			

			//State
			$domElement .= "<div class='col-sm-4'>";
				$domElement .= "<label>".$state['label']."</label><br/>";										
				$domElement .= $this->inputBuilder($state);
			$domElement .= "</div>";						

			//Zip
			$domElement .= "<div class='col-sm-4'>";
				$domElement .= "<label>".$zip['label']."</label><br/>";										
				$domElement .= $this->inputBuilder($zip);
			$domElement .= "</div>";


			///Property Value, if it's part of the template it's required
			if($value != null){
				$domElement .= "<div class='col-sm-6'>";
					$domElement .= "<label>".$value['label']."</label><br/>";										
					$domElement .= $this->inputBuilder($value);
				$domElement .= "</div>";
			}

			///Mortgage, if it's part of the template it's required
			if($mortgage != null){
				$domElement .= "<div class='col-sm-6'>";
					$domElement .= "<label>".$mortgage['label']."</label><br/>";										
					$domElement .= $this->inputBuilder($mortgage);
				$domElement .= "</div>";				
			}				
			
			$domElement .= "</div>";
			return $domElement;			
	}

	public function elementCanvas($inputObj){
		$domElement = "";		
		$domElement .= "<div class='col-sm-12'>".$inputObj['DOM']."</div>";
		return $domElement;
	}

	public function elementTable($inputObj){
		$domElement = "";

		$domElement .="<h2>".$inputObj['label']."</h2>";
		$domElement .="<p>".$inputObj['notes']."</p>";
		$domElement .="<p>".$inputObj['cta']."</p>";
		$domElement .="<table id='".$inputObj['id']."' class='table table-striped m-b-none text-small ".$inputObj['class']." '  data-accredify-template-key='".$inputObj['template']['key']."' data-accredify-template-value='".json_encode($inputObj['template']['value'])."' >";
			$domElement .="<thead>";
			$domElement .="<tr>";
			foreach($inputObj['thead'] as $thead):
					$domElement .="<th>".$thead."</th>";
			endforeach;
			$domElement .="<tr>";
			$domElement .="</thead>";
			$domElement .="<tbody>";
			$domElement .="</tbody>";
		$domElement .="</table>";

		return $domElement;
	}

	public function elementContainer($inputObj){
		$domElement = "";		

		$domElement .= "<div id='".$inputObj['id']."'  class='".$inputObj['class']."' data-accredify-template-key='".$inputObj['template']['key']."' data-accredify-template-value='".json_encode($inputObj['template']['value'])."'>";
			$domElement .= "<h2>".$inputObj['label']."</h2>";
			$domElement .= "<p>".$inputObj['notes']."</p>";
			$domElement .= "<p>".$inputObj['cta']."</p>";
		$domElement .= "</div>";
		return $domElement;
	}

}