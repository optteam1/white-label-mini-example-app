<?php
include 'AccredifyPHP/Accredify.php';
include 'helpers.php';//Basic Helpers

use Accredify\API as AccredifyAPI;
$AccredifyAPI = new AccredifyAPI;
$Helpers = new AccredifyHelpers($AccredifyAPI);
$elements = $Helpers->geUserQuestions(true);

//Data Posted
if(isset($_REQUEST['accredify'])){	
	$results = $AccredifyAPI::newUser();//Signed oAuth2 Request :: Create User
	echo "<pre>";
	print_R($results);
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Twitter Bootstrap (Not Required) -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<style>body {padding-top: 60px;}</style>

	<!-- Required if you're using Accredify/Parsley for Form Validation -->
	<link rel="stylesheet" href="assets/parsley.css">

</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top"></div>
	<div class="container">
		<form id="AccredifyNewUser" method="post">			
		<div class="row">
		
			<div class="col-xs-12">
					<h1>User Data</h1>				
					<h1><?= $elements['section'][0]['label'];?></h1>
					<p><?= $elements['section'][0]['notes'];?></p>
					<div class='row'>
						<?php foreach($elements['section'][0]['elements'] as $element):?>
							<?= $Helpers->inputBuilder($element,true,'col-sm-12');?>
						<?php endforeach;?>											
					</div>
					
					
			</div>

			<div class="col-xs-12" style="text-align:center">
				<input type="submit" value="Create User">
			</div>
		</div>		
		</form>		
	</div> <!-- /container -->


	
		<!-- Accredify White Label Required JS -->
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>  <!-- jQuery (1.1+) -->

	<!-- If you intend to use Accredify as the form validator -->	
	<script type="text/javascript">
	    var accredify_params  = {form:"#AccredifyNewUser", type:"new_user"};		
	   (function() {
	      var accSrc = document.createElement("script"); accSrc.type = "text/javascript";
	      accSrc.async = true;
	      accSrc.src = "/assets/AccredifyWL.js";
	      var s = document.getElementsByTagName("script")[0];
	      s.parentNode.insertBefore(accSrc, s);
	   })();
	</script>
	
	<!-- Twitter Bootstrap (Not Required-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	


</body>
</html>
