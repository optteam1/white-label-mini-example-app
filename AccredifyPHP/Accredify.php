<?php
namespace Accredify;
/* 
*
* NOT FOR PRODUCITON USE
* Used to illustrate basic oAuth2 Requests to and from Accredify.
* No framework used to boil down to the basics. 
*/
include 'vendor/autoload.php';
$dotenv = new \Dotenv\Dotenv(__DIR__);
$dotenv->load();//Load .env File

//Required Libraries
use \GuzzleHttp\Client as GC;
use \OAuth2\Client as Client;//Simple oAuth2 Client

//DEBUGING ALWAYS ON, AS THIS IS NOT FOR PRODUCTION
error_reporting(E_ALL);
ini_set('display_errors', 1);

class API{
	private static $oAuth2 = null;
	public static $config = array();
	private  static $base_url;

	public function __construct(){
		 self::$base_url = ($_ENV['APP_ENV'] == 'sandbox')? "https://api.sandbox.accredify.com/" : "https://api.accredify.com/";
		self::$oAuth2 = new Client($_ENV['APP_ID'], $_ENV['APP_SECRET']);
	}

	//Send Requet to Accredify
	public static function send($multipart,$url){		
  		$client = new \GuzzleHttp\Client();			
		$guzzleResponse = $client->request('POST', $url,	[ 'multipart' => $multipart]);	
                   // echo "<pre>"; print_R(json_encode(json_decode($guzzleResponse->getBody(),true)));exit();
	            return json_decode($guzzleResponse->getBody(),true);
  	}

//START USER CALLS

  	//Pulls Question Set For New User
	public static function newUserQuestions(){
		$response = self::$oAuth2->getAccessToken(self::$base_url.'oauth/token', 'client_credentials',  []);		//Client Signed		
		$url = self::$base_url.'api/wl/user/newQuestions.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $response['result']['access_token']
				];			
		return API::send($multipart,$url);						
  	} 

	//Creates a new user on Accredify  	
	public static function newUser(){					
		$response = self::$oAuth2->getAccessToken(self::$base_url.'oauth/token', 'client_credentials',  []);//Client Signed Request			
		$url = self::$base_url.'api/wl/user/new.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' =>$response['result']['access_token']
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];		
		return API::send($multipart,$url);						
	}

  	//Pulls Question Set For Existing User
	public static function updateUserQuestions($access_token){	
		$url = self::$base_url.'api/wl/user/updateQuestions.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' =>  $access_token
				];		
		return API::send($multipart,$url);					
  	}

	//Updates a user profile data on Accredify
	public static function updateUser($access_token){		
		$url = self::$base_url.'api/wl/user/update.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];		
		return API::send($multipart,$url);	
	}

	//Get user  details from Accredify
	public static function getUser($access_token){
		$url = self::$base_url.'api/wl/user/get.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];		
		return API::send($multipart,$url);	
	}

	//Returns a list of synced users.
	public static function getUserList($page=0,$count=10){			
		$response = self::$oAuth2->getAccessToken(self::$base_url.'oauth/token', 'client_credentials',  []);//Client Signed		
		$url = self::$base_url.'api/wl/user/list.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $response['result']['access_token']
				];
		$multipart[] = [
				'name'     => 'page',
				'contents' =>strval($page)
				];
		$multipart[] = [
				'name'     => 'count',
				'contents' =>strval($count)
				];
		return API::send($multipart,$url);			
    	}

	//Triggers User Webhook
	public static function triggerUserWebhook($hash=null){		
		$response = self::$oAuth2->getAccessToken(self::$base_url.'oauth/token', 'client_credentials',  []);		//Client Signed		
		$url = self::$base_url.'api/wl/user/webhook.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $response['result']['access_token']
				];
		$multipart[] = [
				'name'     => 'hash',
				'contents' => $hash
				];		
		return API::send($multipart,$url);	
  	}

//END USER CALLS
	

//START VERIFICATION CALLS
	
	//Pulls Question Set For $type verification form, includes $access_token to pre-fill any question data we already have 
	public static function getQuestions($type,$access_token){		
		$url = self::$base_url.'api/wl/certificate/'.$type.'/questions.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];		
		return API::send($multipart,$url);					
  	} 

  	public static function newAsset($access_token){		
		$url = self::$base_url.'api/wl/certificate/asset/new.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];
		foreach($_FILES as $name=>$info){
			$multipart[] = [
						'name'     => $name,
						'contents' => fopen( $info['tmp_name'], 'r'),
						'filename' => $info['name']
					];
		}
		return API::send($multipart,$url);					
  	}


  	//Post New Third Party Upload
  	public static function newThirdPartyUpload($access_token){
  		$url = self::$base_url.'api/wl/certificate/thirdparty_upload/new.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];
		foreach($_FILES as $name=>$info){
			$multipart[] = [
						'name'     => $name,
						'contents' => fopen( $info['tmp_name'], 'r'),
						'filename' => $info['name']
					];
		}
		return API::send($multipart,$url);		
  	}


	//Post New Third Party Request
  	public static function newThirdPartyRequest($access_token){
  		$url = self::$base_url.'api/wl/certificate/thirdparty_request/new.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];
		
		return API::send($multipart,$url);		
  	}

	//Post New Entity Upload
	public static function newEntityUpload($access_token){
		$url = self::$base_url.'api/wl/certificate/entity_upload/new.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];
		foreach($_FILES as $name=>$info){
			$multipart[] = [
						'name'     => $name,
						'contents' => fopen( $info['tmp_name'], 'r'),
						'filename' => $info['name']
					];
		}
		return API::send($multipart,$url);
  	}

	//Post New Enity Request
  	public static function newEntityRequest($access_token){
  		$url = self::$base_url.'api/wl/certificate/entity_request/new.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];		
		return API::send($multipart,$url);		
  	}

  	//Post New Income
  	public static function newIncomeRequest($access_token){
  		$url = self::$base_url.'api/wl/certificate/income_request/new.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $access_token
				];
		$multipart[] = [
				'name'     => 'data',
				'contents' => json_encode($_REQUEST)
				];		
		return API::send($multipart,$url);		
  	}

//END VERIFICATION CALLS

//START BILLING CALLS
	
  	//Get Billing Details
  	public static function getBillingDetails(){
		$response = self::$oAuth2->getAccessToken(self::$base_url.'oauth/token', 'client_credentials',  []);	//Client Signed Request	  		
		$url = self::$base_url.'api/wl/billing/details.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $response['result']['access_token']
				];			
		return API::send($multipart,$url);  						
  	}

  	public static function getBillingHistory(){	
		$response = self::$oAuth2->getAccessToken(self::$base_url.'oauth/token', 'client_credentials',  []);	//Client Signed Request	  		
		$url = self::$base_url.'api/wl/billing/history.json';
		$multipart = [];
		$multipart[] = [
				'name'     => 'access_token',
				'contents' => $response['result']['access_token']
				];			
		return API::send($multipart,$url);  						
  	}
//END BILLING CALLS

}
?>
