<?php
include 'AccredifyPHP/Accredify.php';
use Accredify\API as AccredifyAPI;
$AccredifyAPI = new AccredifyAPI;
if(isset($_REQUEST['access_token'])){
	$results = $AccredifyAPI::getUser($_REQUEST['access_token']);//Signed oAuth2 Request :: Get User Details
	echo "<pre>";
	print_R($results);
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Twitter Bootstrap (Not Required) -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<style>body {padding-top: 60px;}</style>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top"></div>
	<div class="container">
		<form id="AccredifyNewUser" method="post">			
		<div class="row">

			<div class="col-xs-12">
				<h1>oAuth2 Access Token</h1>				
				<p>Traditionally stored in DB</p>
				<div class='row'>
					<label>Access Token</label>
					<!-- Input field name does not matter, since you would not be submiting this informaiton via a form -->
					<input type='text' name='access_token' placeholder='Access Token' data-parsley-required='true'>												
				</div>					
			</div>

			

			<div class="col-xs-12" style="text-align:center; margin-top:20px;">
				<input type="submit" value="Get User">
			</div>
		</div>		
		</form>		
	</div> <!-- /container -->
	

	<!-- Twitter Bootstrap (Not Required-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>