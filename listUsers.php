<?php
include 'AccredifyPHP/Accredify.php';
use Accredify\API as AccredifyAPI;
  $AccredifyAPI = new AccredifyAPI;  
$syncedUsers = $AccredifyAPI::getUserList(0,25);//Signed oAuth2 Request :: List Users (page, per page)
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Twitter Bootstrap (Not Required) -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<style>body {padding-top: 60px;}</style>

</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top"></div>
	<div class="container">
	
	<h1>Total Users: <?= $syncedUsers['data']['pagination']['user_count'];?></h1>
	<div class="row">
		<div class="col-sm-4">
			<h4>Users Per Page: <?= $syncedUsers['data']['pagination']['limit'];?></h4>
		</div>
		<div class="col-sm-4">		
			<h4>Current Page: <?= $syncedUsers['data']['pagination']['current_page']+1;?></h4>
		</div>
		<div class="col-sm-4">
			<h4>Total Page(s): <?= $syncedUsers['data']['pagination']['pages']+1;?></h4>
		</div>
	</div>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>DOB</th>
				<th>Status: Message</th>
				<th>Method</th>
				<th>Expires</th>
				<th>Certificate</th>
			</tr> 
		</thead>

		
		<tbody>
			<?php foreach($syncedUsers['data']['users'] as $user):?>
			<tr class='<?= ($user['status']['accredited'])? 'success' : 'warning';?>'>
				<td><?= $user['first_name'];?></td>
				<td><?= $user['last_name'];?></td>
				<td><?= $user['email'];?></td>				
				<td><?= ($user['dob'] != null || $user['dob'] != 0)? date('m/d/Y',$user['dob']) : "---";?></td>
				<td><?= $user['status']['message'];?></td>
				<td><?= ($user['status']['verification_type'] != null)? $user['status']['verification_type'] : '---';?></td>
				<td><?= ($user['status']['expires_on'] != null)? date('m/d/Y',$user['status']['expires_on']) : '---';?></td>
				<td><?= ($user['status']['certificate_url'] != null)? '<a class="btn btn-mini btn-success" href="'.$user['status']['certificate_url'].'" target="_blank">View Certificate</a>' : '<a class="btn btn-mini btn-success" disabled>View Certificate</a>';?></td>
				<td><a class="btn btn-mini btn-success" href="/triggerWebhook.php?hash=<?= $user['hash'];?>" target="_blank">Trigger Webhook</a></td>
			</tr>
			<?php endforeach;?>
		</tbody>
	
			
	</div> <!-- /container -->
	
	<!-- Twitter Bootstrap (Not Required-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>