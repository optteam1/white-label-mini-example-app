<?php
include 'AccredifyPHP/Accredify.php';
use Accredify\API as AccredifyAPI;
  $AccredifyAPI = new AccredifyAPI;
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Twitter Bootstrap (Not Required) -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

</head>
<body>

	<div class="container">

		<h1>Backend &amp; User Functions</h1>
		<ul>
			<li><a href="/billing.php" target="_blank">Billing Info</a></li>
			<li><a href="/listUsers.php" target="_blank">List Synced Users</a></li>
			<li><a href="/newUser.php" target="_blank">Create User (Example Form)</a></li>
			<li><a href="/getUser.php" target="_blank">Pull User Info</a></li>
			<li><a href="/newForm.php?form_type=update_user" target="_blank">Update User</a></li>

		</ul>
	

		<h1>Verification Forms</h1>
		<ul>
			<li><a href="/newForm.php?form_type=income_request" target="_blank">Income</a></li>
			<li><a href="/newForm.php?form_type=asset" target="_blank">Asset</a></li>
			<li><a href="/newForm.php?form_type=thirdparty_upload" target="_blank">Third Party Upload</a></li>
			<li><a href="/newForm.php?form_type=thirdparty_request" target="_blank">Third Party Request</a></li>
			<li><a href="/newForm.php?form_type=entity_upload" target="_blank">Entity/Trust Upload</a></li>
			<li><a href="/newForm.php?form_type=entity_request" target="_blank">Entity/Trust Request</a></li>

		</ul>
	</div> <!-- /container -->


	
	<!-- Accredify White Label Required JS -->
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>  <!-- jQuery (1.1+) -->
	<script src="AccredifyWL.js"></script>
	
	<!-- Twitter Bootstrap (Not Required-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<script type="text/javascript">
	$( document ).ready(function() {
	});
	</script>


</body>
</html>