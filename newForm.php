<?php
//Data Posted
if(isset($_REQUEST['access_token']) && strlen($_REQUEST['access_token']) != 0){
	$path ='';
	switch($_REQUEST['form_type']){
		case "income_request":
			$path = 'newIncome.php';
		break;
		case "asset":
			$path = 'newAssets.php';			
		break;			
		case "thirdparty_request":
			$path = 'newThirdPartyRequest.php';			
		break;
		case "thirdparty_upload":
			$path = 'newThirdPartyUpload.php';			
		break;
		case "entity_request":
			$path = 'newEntityRequest.php';			
		break;
		case "entity_upload":
			$path = 'newEntityUpload.php';			
		break;
		case "update_user":
			$path = 'updateUser.php';			
		break;
	}
	header( 'Location: /'.$path.'?access_token='.$_REQUEST['access_token'] ) ; 	
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Twitter Bootstrap (Not Required) -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<style>body {padding-top: 60px;}</style>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top"></div>
	<div class="container">
		<form method="get"  enctype="multipart/form-data">			
		<div class="row">
			<div class="col-xs-6">
				<h1>oAuth2 Access Token</h1>				
				<p>Traditionally stored in DB</p>
				<div class='row'>
					<label>Access Token</label>
					<!-- Input field name does not matter, since you would not be submiting this informaiton via a form -->
					<input type='text' name='access_token' placeholder='Access Token' data-parsley-required='true'>												
				</div>					
			</div>

			<div class="col-xs-6">
				<h1>Form Type</h1>				
				<p>Income, Asset, Entity</p>
				<div class='row'>
					<label>Form Type</label>

					<select name="form_type">
						<option value="update_user" <?php echo (isset($_REQUEST['form_type']) && $_REQUEST['form_type'] == "update_user")? "selected='selected'" : ""; ?> >Update User</option>					
						<option value="income_request" <?php echo (isset($_REQUEST['form_type']) && $_REQUEST['form_type'] == "income_request")? "selected='selected'" : ""; ?> >Income Request</option>
						<option value="asset" <?php echo (isset($_REQUEST['form_type']) && $_REQUEST['form_type'] == "asset")? "selected='selected'" : ""; ?> >Asset Based</option>						
						<option value="thirdparty_request" <?php echo (isset($_REQUEST['form_type']) && $_REQUEST['form_type'] == "thirdparty_request")? "selected='selected'" : ""; ?> >Third Party Request</option>
						<option value="thirdparty_upload" <?php echo (isset($_REQUEST['form_type']) && $_REQUEST['form_type'] == "thirdparty_upload")? "selected='selected'" : ""; ?> >Third Party Upload</option>
						<option value="entity_request" <?php echo (isset($_REQUEST['form_type']) && $_REQUEST['form_type'] == "entity_request")? "selected='selected'" : ""; ?> >Entity Request</option>
						<option value="entity_upload" <?php echo (isset($_REQUEST['form_type']) && $_REQUEST['form_type'] == "entity_upload")? "selected='selected'" : ""; ?> >Entity Upload</option>
					</select>
				</div>					
			</div>
		
			

			<div class="col-xs-12" style="text-align:center; margin-top:30px;">
				<input type="submit" value="Get Started">
			</div>
		</div>		
		</form>		
	</div> <!-- /container -->


	
	<!-- Accredify White Label Required JS -->
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>  <!-- jQuery (1.1+) -->
	
	<!-- Twitter Bootstrap (Not Required-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	


</body>
</html>